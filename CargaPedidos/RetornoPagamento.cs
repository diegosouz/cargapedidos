﻿using CargaPedidos.Interfaces;
using Dapper;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CargaPedidos
{
    public class RetornoPagamento : IRetornoPagamento
    {
        public RetornoPagamentoModel GetData(string connectionString, string scriptSql)
        {
            using (OracleConnection con = new OracleConnection(connectionString))
            {
                var result = con.Query<RetornoPagamentoModel>(scriptSql).SingleOrDefault();
                return result;
            }
        }

        public void InsertData(string connectionString, string scriptSql, RetornoPagamentoModel model)
        {
            using (OracleConnection con = new OracleConnection(connectionString))
            {
                try
                {
                    con.Execute(scriptSql, model);
                }
                catch (Exception)
                {
                    return;
                }
            }
        }

        public IEnumerable<RetornoPagamentoModel> GetDataItens(string connectionString, string scriptSql)
        {
            using (OracleConnection con = new OracleConnection(connectionString))
            {
                var result = con.Query<RetornoPagamentoModel>(scriptSql).ToList();
                return result;
            }
        }
    }
}
