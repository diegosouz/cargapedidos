﻿using System;

namespace CargaPedidos
{
    public class ItemNotaFiscalModel : IItemNotaFiscalModel
    {
        public int NI_SKU { get; set; }
        public int NI_NF { get; set; }
        public int QT_ITEM { get; set; }
        public decimal VL_VND { get; set; }
        public decimal VL_UNI { get; set; }
        public decimal VL_DCT { get; set; }
        public string CD_SEQ { get; set; }
        public decimal VL_FRT { get; set; }
    }
}
