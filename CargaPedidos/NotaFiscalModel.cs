﻿using System;

namespace CargaPedidos
{
    public class NotaFiscalModel : INotaFiscalModel
    {
        public int NI_NF { get; set; }
        public string NU_NF { get; set; }
        public string NU_SER_NF { get; set; }
        public int NI_TIP_NF { get; set; }
        public int NI_PED { get; set; }
        public int NI_TRS { get; set; }
        public DateTime?  DT_EMS { get; set; }
        public decimal VL_TOT_NF { get; set; }
        public int NI_PES { get; set; }
        public int NI_END { get; set; }
        public string CD_DANFE { get; set; }
        public int NI_SIT_NF { get; set; }
        public decimal VL_FRT { get; set; }
        public decimal VL_DCT { get; set; }
        public int NI_END_PED { get; set; }
        public int CD_TIP_OP { get; set; }
    }
}
