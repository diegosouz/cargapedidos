﻿using CargaPedidos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos
{
    public class AtendimentoModel : IAtendimentoModel
    {
        public decimal NI_ATD { get; set; }
        public DateTime? TS_CAD { get; set; }
        public int NI_PED { get; set; }
        public int NI_SIT_ATD { get; set; }
        public int NI_PES { get; set; }
        public decimal NI_CLS_ATD { get; set; }
        public int NI_CNL_ORI { get; set; }
        public int NI_ATD_REF { get; set; }
        public decimal CD_PRT { get; set; }
        public DateTime? DT_ATD { get; set; }
        public DateTime? DT_ULT_INT { get; set; }
        public DateTime? DT_FIM { get; set; }
        public char? DS_OBS { get; set; }
        public decimal NI_MOT { get; set; }
        public string CD_ORI_ATD { get; set; }
        public DateTime? DT_NOVA_ENTREGA { get; set; }
    }
}
