﻿using System;

namespace CargaPedidos
{
    public class ProdutoModel : IProdutoModel
    {
        public string NI_PRD { get; set; }
        public int NI_MDL { get; set; }
        public int NI_CAT_PTF { get; set; }
        public int NI_MRC { get; set; }
        public int CD_PRD_PLTF { get; set; }

        public string NO_PRD { get; set; }
        public string TX_LINK { get; set; }

        public char IN_EXB_SITE { get; set; }
        public char IN_ATV { get; set; }
        public DateTime? DT_LCM { get; set; }
        public char IN_ATLZ { get; set; }

        public int NI_SIT { get; set; }
        public decimal VL_PRC_CST { get; set; }
        public decimal VL_PRC_VND { get; set; }
        public char IN_QLD { get; set; }
        public char IN_ATLZ_FOT { get; set; }
        public char IN_ATLZ_PRC { get; set; }
        public char IN_PDD { get; set; }
        public decimal VL_PRC_ORI { get; set; }
        public DateTime? DT_ATLZ_PRC { get; set; }

        public int NI_CND_COM { get; set; }
        public char IN_EXB_SEM_ETQ { get; set; }
        public char IN_PRD_FIS { get; set; }
        public char IN_PRE_VND { get; set; }
        public char IN_MKT_PLC { get; set; }
        public char IN_ATLZ_ESP { get; set; }
        public char IN_ATLZ_MED { get; set; }

        public int NI_SEL { get; set; }
        public int NI_CAT_PTF_REF { get; set; }
        public char IN_ATLZ_SKU { get; set; }
        public string DS_META { get; set; }
        public string DS_PRD { get; set; }
        public string DS_SML { get; set; }
        public string DS_PLV_CHV { get; set; }
        public string DS_SRT_DESC { get; set; }
        public char IN_BLQ { get; set; }
        public int NI_SCR { get; set; }
    }
}
