﻿using Dapper;
using CargaPedidos.Interfaces;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Linq;

namespace CargaPedidos
{
    public class Produto : IProduto
    {
        public ProdutoModel GetData(string connectionString, string scriptSql)
        {
            using (OracleConnection con = new OracleConnection(connectionString))
            {
                var result = con.Query<ProdutoModel>(scriptSql).SingleOrDefault();
                return result;
            }
        }

        public void InsertData(string connectionString, string scriptSql, ProdutoModel model)
        {
            using (OracleConnection con = new OracleConnection(connectionString))
            {
                try
                {
                    con.Execute(scriptSql, model);
                }
                catch (Exception)
                {
                    return;
                }
            }
        }
    }
}
