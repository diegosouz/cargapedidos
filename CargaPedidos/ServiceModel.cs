﻿using System;

namespace CargaPedidos
{
    public class ServiceModel : IServiceModel
    {
        public int NI_PED { get; set; }
        public int NI_SEL { get; set; }
        public int NI_PES { get; set; }
        public int NI_SIT_PED { get; set; }

        public string CD_PED_PLTF { get; set; }
        public string DS_ORI { get; set; }

        public DateTime? DT_CAD { get; set; }
        public DateTime? DT_ULT_ALT { get; set; }

        public decimal VL_TOT_PED { get; set; }

        public DateTime? DT_ENV { get; set; }
        public DateTime? DT_PGTO { get; set; }

        public decimal VL_TOT_IMP { get; set; }
        public decimal VL_TOT_ITEM { get; set; }
        public decimal VL_TOT_DCT { get; set; }
        public decimal VL_TOT_FRT { get; set; }

        public string CD_END_PLTF { get; set; }

        public decimal NI_CAN_VND { get; set; }
        public decimal CD_SEQ_PLTF { get; set; }

        public DateTime? DT_ULT_EXEC { get; set; }

        public string CD_SIT_PED { get; set; }
        public string CD_SIT_ENT { get; set; }

        public DateTime? DT_CAD_TZ { get; set; }

        public decimal VL_TOT_PESO_REAL { get; set; }

        public string DS_TIP { get; set; }
    }
}
