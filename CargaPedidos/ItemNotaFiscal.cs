﻿using Dapper;
using CargaPedidos.Interfaces;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CargaPedidos
{
    public class ItemNotaFiscal : IItemNotaFiscal
    {
        public ItemNotaFiscalModel GetData(string connectionString, string scriptSql)
        {
            using (OracleConnection con = new OracleConnection(connectionString))
            {
                var result = con.Query<ItemNotaFiscalModel>(scriptSql).SingleOrDefault();
                return result;
            }
        }

        public IEnumerable<ItemNotaFiscalModel> GetDataItens(string connectionString, string scriptSql)
        {
            using (OracleConnection con = new OracleConnection(connectionString))
            {
                var result = con.Query<ItemNotaFiscalModel>(scriptSql).ToList();
                return result;
            }
        }

        public void InsertData(string connectionString, string scriptSql, ItemNotaFiscalModel model)
        {
            using (OracleConnection con = new OracleConnection(connectionString))
            {
                try
                {
                    con.Execute(scriptSql, model);
                }
                catch (Exception)
                {
                    return;
                }
            }
        }

    }
}
