﻿using CargaPedidos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos
{
    public class PagamentoEstornoModel : IPagamentoEstornoModel
    {
        public int NI_PAG { get; set; }
        public int NI_EST { get; set; }
        public DateTime? TS_PAG_EST { get; set; }
        public decimal VL_EST { get; set; }
        public int QT_PAG_EST_REF { get; set; }
        public char CD_EST_RLZ { get; set; }
        public int? NI_MEIO_PGTO { get; set; }
        public int NI_SIT_PGTO_EST { get; set; }
        public int? NU_BCO { get; set; }
        public char FL_CLI_CON { get; set; }
    }
}
