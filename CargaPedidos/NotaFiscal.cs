﻿using Dapper;
using CargaPedidos.Interfaces;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CargaPedidos
{
    public class NotaFiscal : INotaFiscal
    {
        public NotaFiscalModel GetData(string connectionString, string scriptSql)
        {
            using (OracleConnection con = new OracleConnection(connectionString))
            {
                var result = con.Query<NotaFiscalModel>(scriptSql).SingleOrDefault();
                return result;
            }
        }

        public IEnumerable<NotaFiscalModel> GetDataItens(string connectionString, string scriptSql)
        {
            using (OracleConnection con = new OracleConnection(connectionString))
            {
                var result = con.Query<NotaFiscalModel>(scriptSql).ToList();
                return result;
            }
        }

        public void InsertData(string connectionString, string scriptSql, NotaFiscalModel model)
        {
            using (OracleConnection con = new OracleConnection(connectionString))
            {
                try
                {
                    con.Execute(scriptSql, model);
                }
                catch (Exception)
                {
                    return;
                }
            }
        }
    }
}
