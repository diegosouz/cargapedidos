﻿using System;

namespace CargaPedidos
{
    public class ItemPedidoModel : IItemPedidoModel
    {
        public int NI_PED { get; set; }
        public int NI_SKU { get; set; }
        public string CD_ITEM_PLTF { get; set; } 
        public int QT_ITEM { get; set; } 

        public decimal VL_PRC_VND { get; set; } 
        public decimal VL_PRC_ORI { get; set; } 
        public decimal VL_VND { get; set; } 
        public decimal VL_DCT { get; set; } 
        public decimal VL_FRT_VND { get; set; } 
        public decimal VL_CSS { get; set; } 
        public decimal VL_IMP { get; set; }

        public float VL_FRT_ORI { get; set; }
        public string NI_ITEM_PLTF { get; set; } 
        public decimal NI_ITEM { get; set; }
        public string NI_SKU_EXT { get; set; } 
        public decimal NU_SEQ { get; set; } 
        public decimal VL_DCT_RTO { get; set; } 
        public decimal VL_CMS { get; set; } 
        public decimal PC_CMS { get; set; } 
    }
}
