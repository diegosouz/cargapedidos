﻿using System;

namespace CargaPedidos
{
    public class SkuModel : ISkuModel
    {
        public int NI_SKU { get; set; }
        public string NI_PRD { get; set; }
        public int CD_SKU_PLTF { get; set; }
        public string NO_SKU { get; set; }

        public char IN_ATV_PSV { get; set; }
        public char IN_ATV { get; set; }
        public char IN_EXB_SEM_ETQ { get; set; }
        public char IN_KIT { get; set; }

        public int NU_PED { get; set; }
        public string CD_VC { get; set; }
        public int NI_SIT { get; set; }
        public int QT_STK { get; set; }
        public DateTime? DT_STK { get; set; }

        public char IN_ATLZ { get; set; }
        public decimal VL_PRC_VND { get; set; }
        public char IN_ATLZ_ESP { get; set; }
        public int QT_STK_RSV { get; set; }
        public char IN_ATLZ_STK { get; set; }
        public string NI_SKU_EXT { get; set; }
        public DateTime? DT_PRI_ATV { get; set; }
        public decimal VL_PRC_ORI_SKU { get; set; }
        public string DS_ACE_NEW { get; set; }
        public string DS_ACE { get; set; }
    }
}
