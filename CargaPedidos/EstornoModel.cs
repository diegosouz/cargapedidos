﻿using CargaPedidos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos
{
    public class EstornoModel : IEstornoModel
    {
        public int NI_EST { get; set; }
        public int NI_PES { get; set; }
        public int NI_ATD { get; set; }
        public int NI_TIP_EST { get; set; }
        public DateTime? DT_CAD { get; set; }
        public DateTime? DT_PRS { get; set; }
        public DateTime? DT_EFE { get; set; }
        public decimal VL_EST { get; set; }
        public int NI_NF { get; set; }
        public int NI_SIT_EST { get; set; }
        public char? DS_OBS { get; set; }
        public DateTime? DT_ULT_EXEC { get; set; }
        public string CD_PTC_CAN { get; set; }
        public string DS_OBS_SIS { get; set; }
        public int? NI_TIP_EST_PRE { get; set; }
        public string NU_DOC_SAP { get; set; }
        public string NU_RET_SAP { get; set; }
        public int? NI_REEM { get; set; }
        public string NU_RET_ADQUIRENTE { get; set; }
    }
}
