﻿using CargaPedidos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos
{
    public class RetornoPagamentoModel : IRetornoPagamentoModel
    {
        public int NI_PAG { get; set; }
        public string CD_TID { get; set; }
        public string CD_RET { get; set; }
        public string DS_MSG { get; set; }
        public string CD_AUT { get; set; }
        public string CD_NSU_HOST { get; set; }
        public string CD_NSU_SITE { get; set; }
        public DateTime? DT_RET_PGTO { get; set; }
        public int? NI_ADM { get; set; }
        public string DS_CONNECTOR { get; set; }
    }
}
