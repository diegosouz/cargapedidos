﻿using CargaPedidos.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CargaPedidos
{
    public class Application:IApplication
    {
        private readonly IService service;
        private readonly IClientePedido clientepedido;
        private readonly IMeioPagamento meiopagamento;
        private readonly INotaFiscal notafiscal;
        private readonly IItemNotaFiscal itemnotafiscal;
        private readonly IItemPedido itempedido;
        private readonly ISku sku;
        private readonly IProduto produto;
        private readonly IModelo modelo;
        private readonly ITracking tracking;
        private readonly ITvenEntregaLoop tvenentregaloop;
        private readonly IGiftCardHistorico giftcardhistorico;
        private readonly IGiftCard giftcard;
        private readonly IAtendimento atendimento;
        private readonly IEstorno estorno;
        private readonly IPagamentoEstorno pagamentoEstorno;
        private readonly IRetornoPagamento retornoPagamento;
        private readonly IPagamento pagamento;
        private readonly IReembolso reembolsoService;

        private readonly string dev = "data source=(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = 10.242.102.29)(PORT = 1521))) (CONNECT_DATA = (SERVICE_NAME = Jarvis)));User id=ironman;Password=Ec0mmD3v;Connection LifeTime = 300";

        private readonly string prod = "data source=(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = jarvis.brasil.latam.cea)(PORT = 1521))) (CONNECT_DATA = (SERVICE_NAME = jarvis)));User id=ironman;Password=2uspitrESPa_Unaph0_E;Connection LifeTime = 300";

        public string script = "SELECT NI_PED,NI_SEL, 5951947 AS NI_PES,NI_SIT_PED,CD_PED_PLTF,DS_ORI,DT_CAD," +
                "DT_ULT_ALT,VL_TOT_PED,DT_ENV,DT_PGTO, VL_TOT_IMP,VL_TOT_ITEM,VL_TOT_DCT,VL_TOT_FRT," +
                "CD_END_PLTF,NI_CAN_VND,CD_SEQ_PLTF,DT_ULT_EXEC,CD_SIT_PED,CD_SIT_ENT,DT_CAD_TZ," +
                "VL_TOT_PESO_REAL,DS_TIP FROM TVEN_PEDIDO P WHERE P.CD_PED_PLTF = '";

        public string script3 = "BEGIN INSERT INTO IRONMAN.TVEN_PEDIDO(NI_PED, NI_SEL, NI_PES, NI_SIT_PED, CD_PED_PLTF, DS_ORI, DT_CAD, DT_ULT_ALT, VL_TOT_PED, DT_ENV, DT_PGTO, VL_TOT_IMP, VL_TOT_ITEM, VL_TOT_DCT, VL_TOT_FRT, CD_END_PLTF, NI_CAN_VND, CD_SEQ_PLTF, DT_ULT_EXEC, CD_SIT_PED, CD_SIT_ENT, DT_CAD_TZ, VL_TOT_PESO_REAL, DS_TIP) VALUES (:NI_PED,:NI_SEL, 5951947, :NI_SIT_PED, :CD_PED_PLTF, :DS_ORI, :DT_CAD, :DT_ULT_ALT, :VL_TOT_PED, :DT_ENV, :DT_PGTO, :VL_TOT_IMP, :VL_TOT_ITEM, :VL_TOT_DCT, :VL_TOT_FRT, :CD_END_PLTF, :NI_CAN_VND, :CD_SEQ_PLTF, :DT_ULT_EXEC, :CD_SIT_PED, :CD_SIT_ENT, :DT_CAD_TZ, :VL_TOT_PESO_REAL, :DS_TIP); END;";

        public string selectClientePedido ="SELECT NI_CLI_PED, NI_PED,  DS_EMAIL_PLTF, 'jopessoal@hotmail.com' AS DS_EMAIL,CD_USR_VTEX, 'Joseli' AS NO_PRI_NOM,NO_ULT_NOM, 16239618829 AS CD_DOC,NU_TEL, 5951947 AS NI_PES,NI_VLD_SYNCHRO FROM TVEN_CLIENTE_PEDIDO WHERE NI_PED = ";

        public string selectMeioPagamento = "SELECT * FROM TVEN_PAGAMENTO WHERE NI_PED  = ";

        public string selectNotaFiscal = "SELECT NI_NF,NU_NF,NU_SER_NF,NI_TIP_NF,NI_PED,NI_TRS,DT_EMS,VL_TOT_NF, 5951947 AS NI_PES,NI_END,CD_DANFE,NI_SIT_NF,VL_FRT,VL_DCT,NI_END_PED,CD_TIP_OP FROM TVEN_NOTA_FISCAL WHERE NI_PED = ";

        public string insertNotaFiscal = "BEGIN INSERT INTO TVEN_NOTA_FISCAL (NI_NF, NU_NF, NU_SER_NF, NI_TIP_NF, NI_PED, NI_TRS, DT_EMS, VL_TOT_NF, NI_PES, NI_END, CD_DANFE, NI_SIT_NF, VL_FRT, VL_DCT, NI_END_PED, CD_TIP_OP) VALUES (:NI_NF, :NU_NF, :NU_SER_NF, :NI_TIP_NF, :NI_PED, :NI_TRS, :DT_EMS, :VL_TOT_NF, :NI_PES, :NI_END, :CD_DANFE, :NI_SIT_NF, :VL_FRT, :VL_DCT, :NI_END_PED, :CD_TIP_OP); END;";

        public string selectItemNotaFiscal = "SELECT * FROM IRONMAN.TVEN_NOTA_FISCAL_ITEM WHERE NI_NF =";

        public string insertItemNotaFiscal = "BEGIN INSERT INTO IRONMAN.TVEN_NOTA_FISCAL_ITEM (NI_SKU, NI_NF, QT_ITEM, VL_VND, VL_UNI, VL_DCT, CD_SEQ, VL_FRT) VALUES(:NI_SKU, :NI_NF, :QT_ITEM, :VL_VND, :VL_UNI, :VL_DCT, :CD_SEQ, :VL_FRT); END;";

        public string selectItemPedido = "SELECT * FROM TVEN_PEDIDO_ITEM WHERE NI_PED = ";

        public string insertItemPedido = "BEGIN INSERT INTO IRONMAN.TVEN_PEDIDO_ITEM(NI_PED, NI_SKU, CD_ITEM_PLTF, QT_ITEM, VL_PRC_VND, VL_PRC_ORI, VL_VND, VL_DCT, VL_FRT_VND, VL_CSS, VL_IMP, VL_FRT_ORI, NI_ITEM_PLTF, NI_ITEM, NI_SKU_EXT, NU_SEQ, VL_DCT_RTO, VL_CMS, PC_CMS) VALUES (:NI_PED, :NI_SKU, :CD_ITEM_PLTF, :QT_ITEM, :VL_PRC_VND, :VL_PRC_ORI, :VL_VND, :VL_DCT, :VL_FRT_VND, :VL_CSS, :VL_IMP, :VL_FRT_ORI, :NI_ITEM_PLTF, :NI_ITEM, :NI_SKU_EXT, :NU_SEQ, :VL_DCT_RTO, :VL_CMS, :PC_CMS); END;";

        public string selectSku = "SELECT * FROM TPRD_SKU WHERE NI_SKU = ";

        public string insertSku = "BEGIN INSERT INTO TPRD_SKU(NI_SKU, NI_PRD, CD_SKU_PLTF, NO_SKU, IN_ATV_PSV, IN_ATV, IN_EXB_SEM_ETQ, IN_KIT, NU_PED, CD_VC, NI_SIT, QT_STK, DT_STK, IN_ATLZ, VL_PRC_VND, IN_ATLZ_ESP, QT_STK_RSV, IN_ATLZ_STK, NI_SKU_EXT, DT_PRI_ATV, VL_PRC_ORI_SKU, DS_ACE_NEW, DS_ACE)VALUES(:NI_SKU, :NI_PRD, :CD_SKU_PLTF, :NO_SKU, :IN_ATV_PSV, :IN_ATV, :IN_EXB_SEM_ETQ, :IN_KIT, :NU_PED, :CD_VC, :NI_SIT, :QT_STK, :DT_STK, :IN_ATLZ, :VL_PRC_VND, :IN_ATLZ_ESP, :QT_STK_RSV, :IN_ATLZ_STK, :NI_SKU_EXT, :DT_PRI_ATV, :VL_PRC_ORI_SKU, :DS_ACE_NEW, :DS_ACE); END;";

        public string selectProduto = "SELECT * FROM TPRD_PRODUTO WHERE NI_PRD = ";

        public string insertProduto = "BEGIN INSERT INTO IRONMAN.TPRD_PRODUTO (NI_PRD, NI_MDL, NI_CAT_PTF, NI_MRC, CD_PRD_PLTF, NO_PRD, TX_LINK, IN_EXB_SITE, IN_ATV, DT_LCM, IN_ATLZ, NI_SIT, VL_PRC_CST, VL_PRC_VND, IN_QLD, IN_ATLZ_FOT, IN_ATLZ_PRC, IN_PDD, VL_PRC_ORI, DT_ATLZ_PRC, NI_CND_COM, IN_EXB_SEM_ETQ, IN_PRD_FIS, IN_PRE_VND, IN_MKT_PLC, IN_ATLZ_ESP, IN_ATLZ_MED, NI_SEL, NI_CAT_PTF_REF, IN_ATLZ_SKU, DS_META, DS_PRD, DS_SML, DS_PLV_CHV, DS_SRT_DESC, IN_BLQ, NI_SCR)VALUES(:NI_PRD, :NI_MDL, :NI_CAT_PTF, :NI_MRC, :CD_PRD_PLTF, :NO_PRD, :TX_LINK, :IN_EXB_SITE, :IN_ATV, :DT_LCM, :IN_ATLZ, :NI_SIT, :VL_PRC_CST, :VL_PRC_VND, :IN_QLD, :IN_ATLZ_FOT, :IN_ATLZ_PRC, :IN_PDD, :VL_PRC_ORI, :DT_ATLZ_PRC, :NI_CND_COM, :IN_EXB_SEM_ETQ, :IN_PRD_FIS, :IN_PRE_VND, :IN_MKT_PLC, :IN_ATLZ_ESP, :IN_ATLZ_MED, :NI_SEL, :NI_CAT_PTF_REF, :IN_ATLZ_SKU, :DS_META, :DS_PRD, :DS_SML, :DS_PLV_CHV, :DS_SRT_DESC, :IN_BLQ, :NI_SCR); END;";

        public string selectModelo = "SELECT * FROM TPRD_MODELO WHERE NI_MDL  = ";

        public string insertModelo = "BEGIN INSERT INTO IRONMAN.TPRD_MODELO(NI_MDL, CD_CLS, CD_FNE, CD_CLS_COD, CD_DIV, NO_DIV, CD_DPT, NO_DPT, CD_SDT, NO_SDT, NO_CLS, NOCLSCOD)VALUES(:NI_MDL, :CD_CLS, :CD_FNE, :CD_CLS_COD, :CD_DIV, :NO_DIV, :CD_DPT, :NO_DPT, :CD_SDT, :NO_SDT, :NO_CLS, :NOCLSCOD); END;";

        public string selectTracking = "SELECT * FROM TVEN_TRACKING_PROC_UPLOAD WHERE CD_SEQ_PLTF = ";

        public string insertTracking = "BEGIN INSERT INTO IRONMAN.TVEN_TRACKING_PROC_UPLOAD(CD_SEQ_PLTF, NO_TRACK, DT_BAIXA, PROCESSADO, DT_PROC)VALUES(:CD_SEQ_PLTF, :NO_TRACK, :DT_BAIXA, :PROCESSADO, :DT_PROC); END;";

        public string selectTvenEntregaLoop = "SELECT * FROM TVEN_ENTREGA WHERE NI_PED = ";

        public string insertTvenEntregaLoop = "BEGIN INSERT INTO IRONMAN.TVEN_ENTREGA(NI_ETG, NI_SKU, NI_TRS, NI_TIP_ETG, NO_TIP_ETG, VL_ETG, DT_EST_ETG, NU_NF, NI_PED, NU_ETG_EST, CD_COMPANY)VALUES(:NI_ETG, :NI_SKU, :NI_TRS, :NI_TIP_ETG, :NO_TIP_ETG, :VL_ETG, :DT_EST_ETG, :NU_NF, :NI_PED, :NU_ETG_EST, :CD_COMPANY); END;";

        public string selectGiftCardHistorico = "SELECT NI_GIFT_CRT, 5951947 AS NI_PES, NI_PED, VL_CRG_USO, DT_CAD_REG, CD_SEQ_PLTF, NI_GIFT_STA, FL_SIS FROM TCRM_GIFTCARD_HISTORICO WHERE CD_SEQ_PLTF = ";

        public string selectGiftCard = "SELECT * FROM TCRM_GIFTCARD WHERE NI_GIFT_CRT = ";
        

        public string selectAtendimento = "SELECT NI_ATD, TS_CAD, NI_PED, NI_SIT_ATD, 5951947 AS NI_PES, NI_CLS_ATD, NI_CNL_ORI, NI_ATD_REF, CD_PRT, DT_ATD, DT_ULT_INT, DT_FIM, DS_OBS, NI_MOT, CD_ORI_ATD, DT_NOVA_ENTREGA FROM TCRM_ATENDIMENTO WHERE NI_PED = ";

        public string insertAtendimento = "BEGIN INSERT INTO IRONMAN.TCRM_ATENDIMENTO(NI_ATD, TS_CAD, NI_PED, NI_SIT_ATD, NI_PES, NI_CLS_ATD, NI_CNL_ORI, NI_ATD_REF, CD_PRT, DT_ATD, DT_ULT_INT, DT_FIM, DS_OBS, NI_MOT, CD_ORI_ATD, DT_NOVA_ENTREGA)VALUES(:NI_ATD, :TS_CAD, :NI_PED, :NI_SIT_ATD, :NI_PES, :NI_CLS_ATD, :NI_CNL_ORI, :NI_ATD_REF, :CD_PRT, :DT_ATD, :DT_ULT_INT, :DT_FIM, :DS_OBS, :NI_MOT, :CD_ORI_ATD, :DT_NOVA_ENTREGA); END;";


        public string selectEstorno = "SELECT NI_EST, 5951947 AS NI_PES, NI_ATD, NI_TIP_EST, DT_CAD, DT_PRS, DT_EFE, VL_EST, NI_NF, NI_SIT_EST, DS_OBS, DT_ULT_EXEC, CD_PTC_CAN, DS_OBS_SIS, NI_TIP_EST_PRE, NU_DOC_SAP, NU_RET_SAP, NI_REEM, NU_RET_ADQUIRENTE FROM TCRM_ESTORNO WHERE NI_ATD = ";
                                      
        public string insertEstorno = "BEGIN INSERT INTO IRONMAN.TCRM_ESTORNO(NI_EST, NI_PES, NI_ATD, NI_TIP_EST, DT_CAD, DT_PRS, DT_EFE, VL_EST, NI_NF, NI_SIT_EST, DS_OBS, DT_ULT_EXEC, CD_PTC_CAN, DS_OBS_SIS, NI_TIP_EST_PRE, NU_DOC_SAP, NU_RET_SAP, NI_REEM, NU_RET_ADQUIRENTE)VALUES(:NI_EST, :NI_PES, :NI_ATD, :NI_TIP_EST, :DT_CAD, :DT_PRS, :DT_EFE, :VL_EST, :NI_NF, :NI_SIT_EST, :DS_OBS, :DT_ULT_EXEC, :CD_PTC_CAN, :DS_OBS_SIS, :NI_TIP_EST_PRE, :NU_DOC_SAP, :NU_RET_SAP, :NI_REEM, :NU_RET_ADQUIRENTE); END;";


        public string selectPagamentoEstorno = "SELECT * FROM TCRM_PAGAMENTO_ESTORNO WHERE NI_EST = ";

        public string insertPagamentoEstorno = "BEGIN INSERT INTO IRONMAN.TCRM_PAGAMENTO_ESTORNO(NI_PAG, NI_EST, TS_PAG_EST, VL_EST, QT_PAG_EST_REF, CD_EST_RLZ, NI_MEIO_PGTO, NI_SIT_PGTO_EST, NU_BCO, FL_CLI_CON)VALUES(:NI_PAG, :NI_EST, :TS_PAG_EST, :VL_EST, :QT_PAG_EST_REF, :CD_EST_RLZ, :NI_MEIO_PGTO, :NI_SIT_PGTO_EST, :NU_BCO, :FL_CLI_CON); END;";


        public string selectPagamento = "SELECT * FROM TVEN_PAGAMENTO WHERE NI_PED = ";

        public string insertPagamento = "BEGIN INSERT INTO IRONMAN.TVEN_PAGAMENTO(NI_PAG, NI_MEIO_PGTO, NI_PED, CD_PAG_PLTF, VL_PAG, NU_PAR, VL_REF, NO_IMP_CRT, NU_CRT, DS_URL_BOL, CD_CRT_GIFT, CD_CRT_RED, NO_GRP, CD_TID, DT_VCTO, CD_TRANS_ID, CD_ADQUIRENTE, IN_ATLZ, CD_CONNECTOR)VALUES(:NI_PAG, :NI_MEIO_PGTO, :NI_PED, :CD_PAG_PLTF, :VL_PAG, :NU_PAR, :VL_REF, :NO_IMP_CRT, :NU_CRT, :DS_URL_BOL, :CD_CRT_GIFT, :CD_CRT_RED, :NO_GRP, :CD_TID, :DT_VCTO, :CD_TRANS_ID, :CD_ADQUIRENTE, :IN_ATLZ, :CD_CONNECTOR); END;";


        public string selectRetornoPagamento = "SELECT * FROM TVEN_RETORNO_PAGAMENTO WHERE NI_PAG = ";

        public string insertRetornoPagamento = "BEGIN INSERT INTO IRONMAN.TVEN_RETORNO_PAGAMENTO(NI_PAG, CD_TID, CD_RET, DS_MSG, CD_AUT, CD_NSU_HOST, CD_NSU_SITE, DT_RET_PGTO, NI_ADM, DS_CONNECTOR)VALUES(:NI_PAG, :CD_TID, :CD_RET, :DS_MSG, :CD_AUT, :CD_NSU_HOST, :CD_NSU_SITE, :DT_RET_PGTO, :NI_ADM, :DS_CONNECTOR); END;";


        public string scripTcrmReembolso = "SELECT NI_REEM, NI_PED, DT_SOL, NI_REEM_STA, NI_REEM_TIP, FL_ORI_LAN, VL_REEM, CD_PRT, NI_NF, CD_USR, DT_INC, DT_ALT, NU_DOC_SAP, NU_RET_SAP, FL_FIEL, NU_RET_ADQUIRENTE, FL_CLI_CON FROM TCRM_REEMBOLSO WHERE NI_PED = '";

        public string insertTcrmReembolso = "BEGIN INSERT INTO IRONMAN.TCRM_REEMBOLSO (NI_REEM, NI_PED, DT_SOL, NI_REEM_STA, NI_REEM_TIP, FL_ORI_LAN, VL_REEM, CD_PRT, NI_NF, CD_USR, DT_INC, DT_ALT, NU_DOC_SAP, NU_RET_SAP, FL_FIEL, NU_RET_ADQUIRENTE, FL_CLI_CON)" +
                                            "VALUES(:NI_REEM, :NI_PED, :DT_SOL, :NI_REEM_STA, :NI_REEM_TIP, :FL_ORI_LAN, :VL_REEM, :CD_PRT, :NI_NF, :CD_USR, :DT_INC, :DT_ALT, :NU_DOC_SAP, :NU_RET_SAP, :FL_FIEL, :NU_RET_ADQUIRENTE, :FL_CLI_CON); END;";


        public Application(IService service, IClientePedido clientepedido, IMeioPagamento meiopagamento , INotaFiscal notafiscal, 
                           IItemNotaFiscal itemnotafiscal, IItemPedido itempedido, ISku sku, IProduto produto, IModelo modelo, 
                           ITracking tracking, ITvenEntregaLoop tvenentregaloop, IGiftCardHistorico giftcardhistorico, IGiftCard giftcard,
                           IAtendimento atendimento, IEstorno estorno, IPagamentoEstorno pagamentoEstorno, IRetornoPagamento retornoPagamento,
                           IPagamento pagamento, IReembolso reembolsoService)
        {
            this.service = service ?? throw new ArgumentNullException(nameof(service));
            this.clientepedido = clientepedido ?? throw new ArgumentNullException(nameof(clientepedido));
            this.meiopagamento = meiopagamento ?? throw new ArgumentNullException(nameof(meiopagamento));
            this.notafiscal = notafiscal ?? throw new ArgumentNullException(nameof(notafiscal));
            this.itemnotafiscal = itemnotafiscal ?? throw new ArgumentNullException(nameof(itemnotafiscal));
            this.itempedido = itempedido ?? throw new ArgumentNullException(nameof(itempedido));
            this.sku = sku ?? throw new ArgumentNullException(nameof(sku));
            this.produto = produto ?? throw new ArgumentNullException(nameof(produto));
            this.modelo = modelo ?? throw new ArgumentNullException(nameof(modelo));
            this.tracking = tracking ?? throw new ArgumentNullException(nameof(tracking));
            this.tvenentregaloop = tvenentregaloop ?? throw new ArgumentNullException(nameof(tvenentregaloop));
            this.giftcardhistorico = giftcardhistorico ?? throw new ArgumentNullException(nameof(giftcardhistorico));
            this.giftcard = giftcard ?? throw new ArgumentNullException(nameof(giftcard));
            this.atendimento = atendimento ?? throw new ArgumentNullException(nameof(atendimento));
            this.estorno = estorno ?? throw new ArgumentNullException(nameof(estorno));
            this.pagamentoEstorno = pagamentoEstorno ?? throw new ArgumentNullException(nameof(pagamentoEstorno));
            this.retornoPagamento = retornoPagamento ?? throw new ArgumentNullException(nameof(retornoPagamento));
            this.pagamento = pagamento ?? throw new ArgumentNullException(nameof(pagamento));
            this.reembolsoService = reembolsoService ?? throw new ArgumentNullException(nameof(reembolsoService));
        }

        public void Run()
        {
            Console.WriteLine("Por favor digite o código do pedido.");
            Utilitarios.CodPed = Console.ReadLine();

            var scriptfinal = script + Utilitarios.CodPed +"'";
            var pedido = service.GetData(prod, scriptfinal);
            service.InsertData(dev, script3, pedido);
            
            var scriptclientepedido = selectClientePedido + pedido.NI_PED;

            var clientepedidoteste = clientepedido.GetData(prod, scriptclientepedido);
            var insertClientePedido = "BEGIN INSERT INTO IRONMAN.TVEN_CLIENTE_PEDIDO(NI_CLI_PED, NI_PED, DS_EMAIL_PLTF, DS_EMAIL, CD_USR_VTEX, NO_PRI_NOM, NO_ULT_NOM, CD_DOC, NU_TEL, NI_PES, NI_VLD_SYNCHRO)VALUES(:NI_CLI_PED,  :NI_PED , :DS_EMAIL_PLTF, :DS_EMAIL, :CD_USR_VTEX, :NO_PRI_NOM, :NO_ULT_NOM, :CD_DOC, :NU_TEL, :NI_PES, :NI_VLD_SYNCHRO); END;";
            clientepedido.InsertData(dev, insertClientePedido, clientepedidoteste);

            var scriptmeiopagamento = selectMeioPagamento + pedido.NI_PED;
            var itens = meiopagamento.GetDataItens(prod, scriptmeiopagamento);

            foreach (var item in itens)
            {
                var insertMeioPagamento = "BEGIN INSERT INTO IRONMAN.TVEN_PAGAMENTO(NI_PAG, NI_MEIO_PGTO, NI_PED, CD_PAG_PLTF, VL_PAG, NU_PAR, VL_REF, NO_IMP_CRT, NU_CRT, DS_URL_BOL, CD_CRT_GIFT, CD_CRT_RED, NO_GRP, CD_TID, DT_VCTO, CD_TRANS_ID, CD_ADQUIRENTE, IN_ATLZ, CD_CONNECTOR)VALUES(:NI_PAG, :NI_MEIO_PGTO, :NI_PED, :CD_PAG_PLTF, :VL_PAG, :NU_PAR, :VL_REF, :NO_IMP_CRT, :NU_CRT, :DS_URL_BOL, :CD_CRT_GIFT, :CD_CRT_RED, :NO_GRP, :CD_TID, :DT_VCTO, :CD_TRANS_ID, :CD_ADQUIRENTE, :IN_ATLZ, :CD_CONNECTOR); END;";
                meiopagamento.InsertData(dev, insertMeioPagamento, item);
            }

            var scriptItemPedido = selectItemPedido + pedido.NI_PED;
            var resultItemPedido = itempedido.GetDataItens(prod, scriptItemPedido);

            foreach (var item in resultItemPedido)
            {
                var scriptSku = selectSku + item.NI_SKU;
                var resultSku = sku.GetData(prod, scriptSku);

                var scriptProduto = selectProduto + "'" + resultSku.NI_PRD + "'";
                var resultProduto = produto.GetData(prod, scriptProduto);

                var scriptModelo = selectModelo + resultProduto.NI_MDL;
                var resultModelo = modelo.GetData(prod, scriptModelo);

                modelo.InsertData(dev, insertModelo, resultModelo);
                produto.InsertData(dev, insertProduto, resultProduto);
                sku.InsertData(dev, insertSku, resultSku);

                itempedido.InsertData(dev, insertItemPedido, item);
            }
            
            var scriptnotafiscal = selectNotaFiscal + pedido.NI_PED;

            var resultnotafiscal = notafiscal.GetDataItens(prod, scriptnotafiscal);
            foreach (var item in resultnotafiscal)
            {
                notafiscal.InsertData(dev, insertNotaFiscal, item);
                var scriptItemNotaFiscal = selectItemNotaFiscal + item.NI_NF;
                var resultItemNotaFiscal = itemnotafiscal.GetDataItens(prod, scriptItemNotaFiscal);
                foreach (var item2 in resultItemNotaFiscal)
                {
                    itemnotafiscal.InsertData(dev, insertItemNotaFiscal, item2);
                }
            }


            var scripttracking = selectTracking + pedido.CD_SEQ_PLTF;
            var trackingteste = tracking.GetDataItens(prod, scripttracking);

            foreach (var item in trackingteste)
            {
                tracking.InsertData(dev, insertTracking, item);
            }

            var scripttvenentregaloop = selectTvenEntregaLoop + pedido.NI_PED;
            var tvenentregaloopteste = tvenentregaloop.GetDataItens(prod, scripttvenentregaloop);

            foreach (var entrega in tvenentregaloopteste)
            {
                tvenentregaloop.InsertData(dev, insertTvenEntregaLoop, entrega);
            }


            var scriptGiftCardHistorico = selectGiftCardHistorico + pedido.CD_SEQ_PLTF;
            var giftcardh = giftcardhistorico.GetDataItens(prod, scriptGiftCardHistorico);
            if (giftcardh != null)
            {
                foreach(var gift in giftcardh)
                {
                    var insertGiftCardHistorico = "BEGIN INSERT INTO IRONMAN.TCRM_GIFTCARD_HISTORICO(NI_GIFT_CRT, NI_PES, NI_PED, VL_CRG_USO, DT_CAD_REG, CD_SEQ_PLTF, NI_GIFT_STA, NI_SKU, FL_SIS, NI_REEMBOLSO, NU_DOC_SAP)VALUES(:NI_GIFT_CRT, :NI_PES, :NI_PED, :VL_CRG_USO, :DT_CAD_REG, :CD_SEQ_PLTF, :NI_GIFT_STA, :NI_SKU, :FL_SIS, :NI_REEMBOLSO, :NU_DOC_SAP); END;";
                    giftcardhistorico.InsertData(dev, insertGiftCardHistorico, gift);
                    var scriptGiftCard = selectGiftCard + gift.NI_GIFT_CRT;
                    var giftcardv = giftcard.GetData(prod, scriptGiftCard);
                    var insertGiftCard = "BEGIN INSERT INTO IRONMAN.TCRM_GIFTCARD(NI_GIFT_CRT, DS_HASH_GIFT_CRT, DT_CAD, IN_ATV, NI_PRD, IN_USO, CD_ORI, NI_PES_TEMP, DS_ORI, ID_TIPO_GIFTCARD)VALUES(:NI_GIFT_CRT, :DS_HASH_GIFT_CRT, :DT_CAD, :IN_ATV, :NI_PRD, :IN_USO, :CD_ORI, :NI_PES_TEMP, :DS_ORI, :ID_TIPO_GIFTCARD); END;";
                    giftcard.InsertData(dev, insertGiftCard, giftcardv);
                }
            }
            else
            {
                Console.WriteLine("Pedido não tem Gift nem Histórico Gift");
            }


            var scriptatendimento = selectAtendimento + pedido.NI_PED;
            var atedimentoteste = atendimento.GetDataItens(prod, scriptatendimento);
            foreach (var item in atedimentoteste)
            {
                if (atedimentoteste != null)
                {
                    Console.WriteLine("\nDeseja também inserir na tabela Estorno? S/N");
                    var inserirEstorno = Console.ReadLine().ToUpper();

                    if (inserirEstorno == "S")
                    {
                        atendimento.InsertData(dev, insertAtendimento, item);

                        var scriptestorno = selectEstorno + item.NI_ATD;
                        var estornoteste = estorno.GetDataItens(prod, scriptestorno);

                        foreach (var cadaEstorno in estornoteste)
                        {
                            estorno.InsertData(dev, insertEstorno, cadaEstorno);

                            var scriptpagamentoestorno = selectPagamentoEstorno + cadaEstorno.NI_EST;
                            var pagamentoestornoteste = pagamentoEstorno.GetDataItens(prod, scriptpagamentoestorno);

                            foreach (var cadaPagamentoEstorno in pagamentoestornoteste)
                            {
                                pagamentoEstorno.InsertData(dev, insertPagamentoEstorno, cadaPagamentoEstorno);
                            }
                        }
                    }
                }                       
            }


            var scriptPagamento = selectPagamento + pedido.NI_PED;
            var resultPagamento = pagamento.GetDataItens(prod, scriptPagamento);

            if (resultPagamento != null)
            {
                foreach (var cadaPagamento in resultPagamento)
                {
                    pagamento.InsertData(dev, insertPagamento, cadaPagamento);

                    var scriptRetornoPagamento = selectRetornoPagamento + cadaPagamento.NI_PAG;
                    var resultRetornoPagamento = retornoPagamento.GetDataItens(prod, scriptRetornoPagamento);

                    if (resultRetornoPagamento != null)
                    {
                        foreach (var cadaResultadoPagamento in resultRetornoPagamento)
                        {
                            retornoPagamento.InsertData(dev, insertRetornoPagamento, cadaResultadoPagamento);
                        }
                    }
                }
            }


            var scriptReembolso = scripTcrmReembolso + pedido.NI_PED + "'";
            var reembolso = reembolsoService.GetData(prod, scriptReembolso);
            if (reembolso.NI_REEM != null)
            {
                Console.WriteLine("\nDeseja também inserir na tabela Reembolso? S/N");
                var inserirNaReembolso = Console.ReadLine().ToUpper();

                if (inserirNaReembolso == "S")
                    reembolsoService.InsertData(dev, insertTcrmReembolso, reembolso);
            }


            Console.WriteLine("Todo o pedido foi inserido com sucesso.");
        }
    }
}
