﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos
{
    public class Utilitarios
    {
        public static string CodPed { get; set; }

        public static string TipoPedido { get; set; }

        public static string MeioPagamento { get; set; }
    }
}
