﻿using Dapper;
using CargaPedidos.Interfaces;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CargaPedidos
{
    public class ItemPedido : IItemPedido
    {
        public ItemPedidoModel GetData(string connectionString, string scriptSql)
        {
            using (OracleConnection con = new OracleConnection(connectionString))
            {
                var result = con.Query<ItemPedidoModel>(scriptSql).SingleOrDefault();
                return result;
            }
        }

        public void InsertData(string connectionString, string scriptSql, ItemPedidoModel model)
        {
            using (OracleConnection con = new OracleConnection(connectionString))
            {
                try
                {
                    con.Execute(scriptSql, model);
                }
                catch (Exception)
                {
                    return;
                }
            }
        }

        public IEnumerable<ItemPedidoModel> GetDataItens(string connectionString, string scriptSql)
        {
            using (OracleConnection con = new OracleConnection(connectionString))
            {
                var result = con.Query<ItemPedidoModel>(scriptSql).ToList();
                return result;
            }
        }
    }
}
