﻿using CargaPedidos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos
{
    public class TrackingModel : ITrackingModel
    {
        public decimal CD_SEQ_PLTF { get; set; }
        public string NO_TRACK { get; set; }
        public DateTime? DT_BAIXA { get; set; }
        public int PROCESSADO { get; set; }
        public DateTime? DT_PROC { get; set; }
    }
}
