﻿using CargaPedidos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos
{
    public class GiftCardHistoricoModel : IGiftCardHistoricoModel
    {
        public int NI_GIFT_CRT { get; set; }
        public int NI_PES { get; set; }
        public int NI_PED { get; set; }
        public decimal VL_CRG_USO { get; set; }
        public DateTime? DT_CAD_REG { get; set; }
        public decimal CD_SEQ_PLTF { get; set; }
        public decimal? NI_GIFT_STA { get; set; }
        public int? NI_SKU { get; set; }
        public char? FL_SIS { get; set; }
        public int? NI_REEMBOLSO { get; set; }
        public string NU_DOC_SAP { get; set; }
    }
}
