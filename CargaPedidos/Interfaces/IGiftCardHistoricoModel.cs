﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos.Interfaces
{
    public interface IGiftCardHistoricoModel
    {
        int NI_GIFT_CRT { get; set; }
        int NI_PES { get; set; }
        int NI_PED { get; set; }
        decimal VL_CRG_USO { get; set; }
        DateTime? DT_CAD_REG { get; set; }
        decimal CD_SEQ_PLTF { get; set; }
        decimal? NI_GIFT_STA { get; set; }
        int? NI_SKU { get; set; }
        char? FL_SIS { get; set; }
        int? NI_REEMBOLSO { get; set; }
        string NU_DOC_SAP { get; set; }
    }
}
