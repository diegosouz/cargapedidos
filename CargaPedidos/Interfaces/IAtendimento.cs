﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos.Interfaces
{
    public interface IAtendimento
    {
        AtendimentoModel GetData(string connectionString, string dailyExportScript);
        IEnumerable<AtendimentoModel> GetDataItens(string connectionString, string dailyExportScript);
        void InsertData(string connectionString, string scriptSql, AtendimentoModel model);
    }
}
