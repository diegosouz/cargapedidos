﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos.Interfaces
{
    public interface IPagamentoEstorno
    {
        PagamentoEstornoModel GetData(string connectionString, string dailyExportScript);
        IEnumerable<PagamentoEstornoModel> GetDataItens(string connectionString, string dailyExportScript);
        void InsertData(string connectionString, string scriptSql, PagamentoEstornoModel model);
    }
}
