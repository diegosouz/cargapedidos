﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using static CargaPedidos.Service;

namespace CargaPedidos.Interfaces
{
    public interface IItemNotaFiscal
    {
        ItemNotaFiscalModel GetData(string connectionString, string dailyExportScript);
        IEnumerable<ItemNotaFiscalModel> GetDataItens(string connectionString, string dailyExportScript);
        void InsertData(string connectionString, string scriptSql, ItemNotaFiscalModel model);
    }
}
