﻿using System;

namespace CargaPedidos
{
    public interface INotaFiscalModel
    {
        int NI_NF { get; set; }
        string NU_NF { get; set; }
        string NU_SER_NF { get; set; }
        int NI_TIP_NF { get; set; }
        int NI_PED { get; set; }
        int NI_TRS { get; set; }
        DateTime?  DT_EMS { get; set; }
        decimal VL_TOT_NF { get; set; }
        int NI_PES { get; set; }
        int NI_END { get; set; }
        string CD_DANFE { get; set; }
        int NI_SIT_NF { get; set; }
        decimal VL_FRT { get; set; }
        decimal VL_DCT { get; set; }
        int NI_END_PED { get; set; }
        int CD_TIP_OP { get; set; }
    }
}
