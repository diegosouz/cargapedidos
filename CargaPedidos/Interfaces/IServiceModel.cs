﻿using System;

namespace CargaPedidos
{
    public interface IServiceModel
    {
         int NI_PED { get; set; }
         int NI_SEL { get; set; }
         int NI_PES { get; set; }
         int NI_SIT_PED { get; set; }

         string CD_PED_PLTF { get; set; }
         string DS_ORI { get; set; }

         DateTime? DT_CAD { get; set; }
         DateTime? DT_ULT_ALT { get; set; }

         decimal VL_TOT_PED { get; set; }

         DateTime? DT_ENV { get; set; }
         DateTime? DT_PGTO { get; set; }

         decimal VL_TOT_IMP { get; set; }
         decimal VL_TOT_ITEM { get; set; }
         decimal VL_TOT_DCT { get; set; }
         decimal VL_TOT_FRT { get; set; }

         string CD_END_PLTF { get; set; }

         decimal NI_CAN_VND { get; set; }
         decimal CD_SEQ_PLTF { get; set; }

         DateTime? DT_ULT_EXEC { get; set; }

         string CD_SIT_PED { get; set; }
         string CD_SIT_ENT { get; set; }

         DateTime? DT_CAD_TZ { get; set; }

         decimal VL_TOT_PESO_REAL { get; set; }

         string DS_TIP { get; set; }
    }
}
