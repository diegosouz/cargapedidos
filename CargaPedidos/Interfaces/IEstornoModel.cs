﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos.Interfaces
{
    public interface IEstornoModel
    {
        int NI_EST { get; set; }
        int NI_PES { get; set; }
        int NI_ATD { get; set; }
        int NI_TIP_EST { get; set; }
        DateTime? DT_CAD { get; set; }
        DateTime? DT_PRS { get; set; }
        DateTime? DT_EFE { get; set; }
        decimal VL_EST { get; set; }
        int NI_NF { get; set; }
        int NI_SIT_EST { get; set; }
        char? DS_OBS { get; set; }
        DateTime? DT_ULT_EXEC { get; set; }
        string CD_PTC_CAN { get; set; }
        string DS_OBS_SIS { get; set; }
        int? NI_TIP_EST_PRE { get; set; }
        string NU_DOC_SAP { get; set; }
        string NU_RET_SAP { get; set; }
        int? NI_REEM { get; set; }
        string NU_RET_ADQUIRENTE { get; set; }
    }
}
