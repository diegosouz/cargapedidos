﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos.Interfaces
{
    public interface IRetornoPagamentoModel
    {
        int NI_PAG { get; set; }
        string CD_TID { get; set; }
        string CD_RET { get; set; }
        string DS_MSG { get; set; }
        string CD_AUT { get; set; }
        string CD_NSU_HOST { get; set; }
        string CD_NSU_SITE { get; set; }
        DateTime? DT_RET_PGTO { get; set; }
        int? NI_ADM { get; set; }
        string DS_CONNECTOR { get; set; }
    }
}
