﻿namespace CargaPedidos.Interfaces
{
    public interface IReembolso
    {
        ReembolsoModel GetData(string connectionString, string dailyExportScript);
        void InsertData(string connectionString, string scriptSql, ReembolsoModel model);
    }
}
