﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos.Interfaces
{
    public interface ITvenEntregaLoop
    {
        TvenEntregaLoopModel GetData(string connectionString, string dailyExportScript);
        IEnumerable<TvenEntregaLoopModel> GetDataItens(string connectionString, string dailyExportScript);
        void InsertData(string connectionString, string scriptSql, TvenEntregaLoopModel model);

    }
}
