﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos.Interfaces
{
    public interface IEstorno
    {
        EstornoModel GetData(string connectionString, string dailyExportScript);
        IEnumerable<EstornoModel> GetDataItens(string connectionString, string dailyExportScript);
        void InsertData(string connectionString, string scriptSql, EstornoModel model);
    }
}
