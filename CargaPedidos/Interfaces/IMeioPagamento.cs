﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using static CargaPedidos.Service;

namespace CargaPedidos.Interfaces
{
    public interface IMeioPagamento
    {
        MeioPagamentoModel GetData(string connectionString, string dailyExportScript);
        void InsertData(string connectionString, string scriptSql, MeioPagamentoModel model);
        IEnumerable<MeioPagamentoModel> GetDataItens(string connectionString, string dailyExportScript);
    }
}
