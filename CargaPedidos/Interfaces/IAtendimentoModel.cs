﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos.Interfaces
{
    public interface IAtendimentoModel
    {
        decimal NI_ATD { get; set; }
        DateTime? TS_CAD { get; set; }
        int NI_PED { get; set; }
        int NI_SIT_ATD { get; set; }
        int NI_PES { get; set; }
        decimal NI_CLS_ATD { get; set; }
        int NI_CNL_ORI { get; set; }
        int NI_ATD_REF { get; set; }
        decimal CD_PRT { get; set; }
        DateTime? DT_ATD { get; set; }
        DateTime? DT_ULT_INT { get; set; }
        DateTime? DT_FIM { get; set; }
        char? DS_OBS { get; set; }
        decimal NI_MOT { get; set; }
        string CD_ORI_ATD { get; set; }
        DateTime? DT_NOVA_ENTREGA { get; set; }
    }
}
