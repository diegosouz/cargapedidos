﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos.Interfaces
{
    public interface IRetornoPagamento
    {
        RetornoPagamentoModel GetData(string connectionString, string dailyExportScript);
        IEnumerable<RetornoPagamentoModel> GetDataItens(string connectionString, string dailyExportScript);
        void InsertData(string connectionString, string scriptSql, RetornoPagamentoModel model);
    }
}
