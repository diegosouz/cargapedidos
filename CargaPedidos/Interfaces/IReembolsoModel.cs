﻿using System;

namespace CargaPedidos.Interfaces
{
    public interface IReembolsoModel
    {
        int? NI_REEM { get; set; }
        int NI_PED { get; set; }
        DateTime? DT_SOL { get; set; }
        int NI_REEM_STA { get; set; }
        int NI_REEM_TIP { get; set; }
        char? FL_ORI_LAN { get; set; }
        decimal VL_REEM { get; set; }
        int CD_PRT { get; set; }
        int NI_NF { get; set; }
        string CD_USR { get; set; }
        DateTime? DT_INC { get; set; }
        DateTime? DT_ALT { get; set; }
        string NU_DOC_SAP { get; set; }
        string NU_RET_SAP { get; set; }
        char? FL_FIEL { get; set; }
        string NU_RET_ADQUIRENTE { get; set; }
        char? FL_CLI_CON { get; set; }
    }
}
