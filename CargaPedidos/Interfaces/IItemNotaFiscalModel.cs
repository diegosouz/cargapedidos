﻿using System;

namespace CargaPedidos
{
    public interface IItemNotaFiscalModel
    {
        int NI_SKU { get; set; }
        int NI_NF { get; set; }
        int QT_ITEM { get; set; }
        decimal VL_VND { get; set; }
        decimal VL_UNI { get; set; }
        decimal VL_DCT { get; set; }
        string CD_SEQ { get; set; }
        decimal VL_FRT { get; set; }
    }
}
