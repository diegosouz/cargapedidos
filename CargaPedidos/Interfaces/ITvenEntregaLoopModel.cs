﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos.Interfaces
{
    public interface ITvenEntregaLoopModel
    {
        decimal NI_ETG { get; set; }
        decimal NI_SKU { get; set; }
        int NI_TRS { get; set; }
        int NI_TIP_ETG { get; set; }
        string NO_TIP_ETG { get; set; }
        decimal VL_ETG { get; set; }
        DateTime? DT_EST_ETG { get; set; }
        int NU_NF { get; set; }
        decimal NI_PED { get; set; }
        string NU_ETG_EST { get; set; }
        string CD_COMPANY { get; set; }
    }
}

