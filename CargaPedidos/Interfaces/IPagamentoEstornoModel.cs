﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos.Interfaces
{
    public interface IPagamentoEstornoModel
    {
        int NI_PAG { get; set; }
        int NI_EST { get; set; }
        DateTime? TS_PAG_EST { get; set; }
        decimal VL_EST { get; set; }
        int QT_PAG_EST_REF { get; set; }
        char CD_EST_RLZ { get; set; }
        int? NI_MEIO_PGTO { get; set; }
        int NI_SIT_PGTO_EST { get; set; }
        int? NU_BCO { get; set; }
        char FL_CLI_CON { get; set; }
    }
}
