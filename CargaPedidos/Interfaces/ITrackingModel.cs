﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos.Interfaces
{
    public interface ITrackingModel
    {
        decimal CD_SEQ_PLTF { get; set; }
        string NO_TRACK { get; set; }
        DateTime? DT_BAIXA { get; set; }
        int PROCESSADO { get; set; }
        DateTime? DT_PROC { get; set; }
    }
}
