﻿using System;

namespace CargaPedidos
{
    public interface IProdutoModel
    {
        string NI_PRD { get; set; }
        int NI_MDL { get; set; }
        int NI_CAT_PTF { get; set; }
        int NI_MRC { get; set; }
        int CD_PRD_PLTF { get; set; }

        string NO_PRD { get; set; }
        string TX_LINK { get; set; }

        char IN_EXB_SITE { get; set; }
        char IN_ATV { get; set; }
        DateTime? DT_LCM { get; set; }
        char IN_ATLZ { get; set; }

        int NI_SIT { get; set; }
        decimal VL_PRC_CST { get; set; }
        decimal VL_PRC_VND { get; set; }
        char IN_QLD { get; set; }
        char IN_ATLZ_FOT { get; set; }
        char IN_ATLZ_PRC { get; set; }
        char IN_PDD { get; set; }
        decimal VL_PRC_ORI { get; set; }
        DateTime? DT_ATLZ_PRC { get; set; }

        int NI_CND_COM { get; set; }
        char IN_EXB_SEM_ETQ { get; set; }
        char IN_PRD_FIS { get; set; }
        char IN_PRE_VND { get; set; }
        char IN_MKT_PLC { get; set; }
        char IN_ATLZ_ESP { get; set; }
        char IN_ATLZ_MED { get; set; }

        int NI_SEL { get; set; }
        int NI_CAT_PTF_REF { get; set; }
        char IN_ATLZ_SKU { get; set; }
        string DS_META { get; set; }
        string DS_PRD { get; set; }
        string DS_SML { get; set; }
        string DS_PLV_CHV { get; set; }
        string DS_SRT_DESC { get; set; }
        char IN_BLQ { get; set; }
        int NI_SCR { get; set; }
    }
}
