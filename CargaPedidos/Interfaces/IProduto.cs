﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using static CargaPedidos.Service;

namespace CargaPedidos.Interfaces
{
    public interface IProduto
    {
        ProdutoModel GetData(string connectionString, string dailyExportScript);
        void InsertData(string connectionString, string scriptSql, ProdutoModel model);
    }
}
