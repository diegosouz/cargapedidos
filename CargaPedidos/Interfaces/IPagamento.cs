﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos.Interfaces
{
    public interface IPagamento
    {
        PagamentoModel GetData(string connectionString, string dailyExportScript);
        void InsertData(string connectionString, string scriptSql, PagamentoModel model);
        IEnumerable<PagamentoModel> GetDataItens(string connectionString, string dailyExportScript);
    }
}
