﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using static CargaPedidos.Service;

namespace CargaPedidos.Interfaces
{
    public interface INotaFiscal
    {
        NotaFiscalModel GetData(string connectionString, string dailyExportScript);
        void InsertData(string connectionString, string scriptSql, NotaFiscalModel model);
        IEnumerable<NotaFiscalModel> GetDataItens(string connectionString, string dailyExportScript);
    }
}
