﻿using System;

namespace CargaPedidos
{
    public interface IItemPedidoModel
    {
        int NI_PED { get; set; }
        int NI_SKU { get; set; }
        string CD_ITEM_PLTF { get; set; }
        int QT_ITEM { get; set; }

        decimal VL_PRC_VND { get; set; }
        decimal VL_PRC_ORI { get; set; }
        decimal VL_VND { get; set; }
        decimal VL_DCT { get; set; }
        decimal VL_FRT_VND { get; set; }
        decimal VL_CSS { get; set; }
        decimal VL_IMP { get; set; } 

        float VL_FRT_ORI { get; set; }
        string NI_ITEM_PLTF { get; set; }
        decimal NI_ITEM { get; set; }
        string NI_SKU_EXT { get; set; }
        decimal NU_SEQ { get; set; }
        decimal VL_DCT_RTO { get; set; }
        decimal VL_CMS { get; set; }
        decimal PC_CMS { get; set; } 
    }
}
