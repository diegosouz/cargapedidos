﻿using System;

namespace CargaPedidos
{
    public interface IModeloModel
    {
        int NI_MDL { get; set; }
        int CD_CLS { get; set; }
        int CD_FNE { get; set; }
        int CD_CLS_COD { get; set; }

        decimal CD_DIV { get; set; }
        string NO_DIV { get; set; }
        decimal CD_DPT { get; set; }
        string NO_DPT { get; set; }
        decimal CD_SDT { get; set; }

        string NO_SDT { get; set; }
        string NO_CLS { get; set; }
        string NOCLSCOD { get; set; }
    }
}
