﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos.Interfaces
{
    public interface IPagamentoModel
    {
        int NI_PAG { get; set; }
        int NI_MEIO_PGTO { get; set; }
        int NI_PED { get; set; }
        string CD_PAG_PLTF { get; set; }
        decimal VL_PAG { get; set; }
        int NU_PAR { get; set; }
        decimal VL_REF { get; set; }
        string NO_IMP_CRT { get; set; }
        string NU_CRT { get; set; }
        string DS_URL_BOL { get; set; }
        string CD_CRT_GIFT { get; set; }
        string CD_CRT_RED { get; set; }
        string NO_GRP { get; set; }
        string CD_TID { get; set; }
        DateTime? DT_VCTO { get; set; }
        string CD_TRANS_ID { get; set; }
        int? CD_ADQUIRENTE { get; set; }
        char? IN_ATLZ { get; set; }
        string CD_CONNECTOR { get; set; }
    }
}
