﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos
{
    public interface IClientePedidoModel
    {
        decimal NI_CLI_PED { get; set; }
        decimal NI_PED { get; set; }
        string DS_EMAIL_PLTF { get; set; }
        string DS_EMAIL { get; set; }
        string CD_USR_VTEX { get; set; }
        string NO_PRI_NOM { get; set; }
        string NO_ULT_NOM { get; set; }
        string CD_DOC { get; set; }
        string NU_TEL { get; set; }
        decimal NI_PES { get; set; }
        decimal NI_VLD_SYNCHRO { get; set; }
    }
}
