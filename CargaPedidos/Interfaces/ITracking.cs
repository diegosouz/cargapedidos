﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos.Interfaces
{
    public interface ITracking
    {
        TrackingModel GetData(string connectionString, string dailyExportScript);
        IEnumerable<TrackingModel> GetDataItens(string connectionString, string dailyExportScript);
        void InsertData(string connectionString, string scriptSql, TrackingModel model);
    }
}
