﻿using System;

namespace CargaPedidos
{
    public interface ISkuModel
    {
        int NI_SKU { get; set; }
        string NI_PRD { get; set; }
        int CD_SKU_PLTF { get; set; }
        string NO_SKU { get; set; }

        char IN_ATV_PSV { get; set; }
        char IN_ATV { get; set; }
        char IN_EXB_SEM_ETQ { get; set; }
        char IN_KIT { get; set; }

        int NU_PED { get; set; }
        string CD_VC { get; set; }
        int NI_SIT { get; set; }
        int QT_STK { get; set; }
        DateTime? DT_STK { get; set; }

        char IN_ATLZ { get; set; }
        decimal VL_PRC_VND { get; set; }
        char IN_ATLZ_ESP { get; set; }
        int QT_STK_RSV { get; set; }
        char IN_ATLZ_STK { get; set; }
        string NI_SKU_EXT { get; set; }
        DateTime? DT_PRI_ATV { get; set; }
        decimal VL_PRC_ORI_SKU { get; set; }
        string DS_ACE_NEW { get; set; }
        string DS_ACE { get; set; }
    }
}
