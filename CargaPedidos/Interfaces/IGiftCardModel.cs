﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos.Interfaces
{
    public interface IGiftCardModel
    {
        int NI_GIFT_CRT { get; set; }
        string DS_HASH_GIFT_CRT { get; set; }
        DateTime? DT_CAD { get; set; }
        char IN_ATV { get; set; }
        string NI_PRD { get; set; }
        string IN_USO { get; set; }
        char CD_ORI { get; set; }
        int NI_PES_TEMP { get; set; }
        string DS_ORI { get; set; }
        decimal ID_TIPO_GIFTCARD { get; set; }
    }
}
