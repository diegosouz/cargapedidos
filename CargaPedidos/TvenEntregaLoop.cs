﻿using CargaPedidos.Interfaces;
using Dapper;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CargaPedidos
{
    public class TvenEntregaLoop : ITvenEntregaLoop
    {
        public TvenEntregaLoopModel GetData(string connectionString, string scriptSql)
        {
            using (OracleConnection con = new OracleConnection(connectionString))
            {
                var result = con.Query<TvenEntregaLoopModel>(scriptSql).SingleOrDefault();
                return result;
            }
        }

        public void InsertData(string connectionString, string scriptSql, TvenEntregaLoopModel model)
        {
            using (OracleConnection con = new OracleConnection(connectionString))
            {
                try
                {
                    con.Execute(scriptSql, model);
                }
                catch (Exception)
                {
                    return;
                }
            }
        }

        public IEnumerable<TvenEntregaLoopModel> GetDataItens(string connectionString, string scriptSql)
        {
            using (OracleConnection con = new OracleConnection(connectionString))
            {
                var result = con.Query<TvenEntregaLoopModel>(scriptSql).ToList();
                return result;
            }
        }
    }
}
