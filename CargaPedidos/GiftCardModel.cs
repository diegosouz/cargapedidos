﻿using CargaPedidos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos
{
    public class GiftCardModel : IGiftCardModel
    {
        public int NI_GIFT_CRT { get; set; }
        public string DS_HASH_GIFT_CRT { get; set; }
        public DateTime? DT_CAD { get; set; }
        public char IN_ATV { get; set; }
        public string NI_PRD { get; set; }
        public string IN_USO { get; set; }
        public char CD_ORI { get; set; }
        public int NI_PES_TEMP { get; set; }
        public string DS_ORI { get; set; }
        public decimal ID_TIPO_GIFTCARD { get; set; }
    }
}
