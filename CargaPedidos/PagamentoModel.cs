﻿using CargaPedidos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos
{
    public class PagamentoModel : IPagamentoModel
    {
        public int NI_PAG { get; set; }
        public int NI_MEIO_PGTO { get; set; }
        public int NI_PED { get; set; }
        public string CD_PAG_PLTF { get; set; }
        public decimal VL_PAG { get; set; }
        public int NU_PAR { get; set; }
        public decimal VL_REF { get; set; }
        public string NO_IMP_CRT { get; set; }
        public string NU_CRT { get; set; }
        public string DS_URL_BOL { get; set; }
        public string CD_CRT_GIFT { get; set; }
        public string CD_CRT_RED { get; set; }
        public string NO_GRP { get; set; }
        public string CD_TID { get; set; }
        public DateTime? DT_VCTO { get; set; }
        public string CD_TRANS_ID { get; set; }
        public int? CD_ADQUIRENTE { get; set; }
        public char? IN_ATLZ { get; set; }
        public string CD_CONNECTOR { get; set; }
    }
}
