﻿using System;
using Microsoft.Extensions.DependencyInjection;
using CargaPedidos.Interfaces;

namespace CargaPedidos
{
    public class Program
    {
        static void Main()
        {
            Console.WriteLine("Bem vindo ao Copy Data");
                    
            var serviceProvider = new ServiceCollection()
                //.AddOptions()
                .AddSingleton<IApplication, Application>()
                .AddSingleton<IService, Service>()
                .AddSingleton<IClientePedido, ClientePedido>()
                .AddSingleton<IMeioPagamento, MeioPagamento>()
                .AddSingleton<INotaFiscal, NotaFiscal>()
                .AddSingleton<IItemNotaFiscal, ItemNotaFiscal>()
                .AddSingleton<IItemPedido, ItemPedido>()
                .AddSingleton<ISku, Sku>()
                .AddSingleton<IProduto, Produto>()
                .AddSingleton<IModelo, Modelo>()
                .AddSingleton<ITracking, Tracking>()
                .AddSingleton<ITvenEntregaLoop, TvenEntregaLoop>()
                .AddSingleton<IGiftCard, GiftCard>()
                .AddSingleton<IGiftCardHistorico, GiftCardHistorico>()
                .AddSingleton<IAtendimento, Atendimento>()
                .AddSingleton<IEstorno, Estorno>()
                .AddSingleton<IPagamentoEstorno, PagamentoEstorno>()
                .AddSingleton<IRetornoPagamento, RetornoPagamento>()
                .AddSingleton<IPagamento, Pagamento>()
                .AddSingleton<IReembolso, Reembolso>()
                .BuildServiceProvider();

            var engine = serviceProvider.GetService<IApplication>();

            try
            {
                engine.Run();
            }
            catch (Exception ex)
            {

                Console.WriteLine();
                Console.WriteLine(DateTime.Now);
                Console.WriteLine("Error : {0}", ex);
                Console.WriteLine();
            }
               
            Console.WriteLine();
            Console.WriteLine("Pressione 'Enter' para continuar ");
            Console.ReadLine();
        }
    }
}
