﻿using System;

namespace CargaPedidos
{
    public class ModeloModel : IModeloModel
    {
        public int NI_MDL { get; set; }
        public int CD_CLS { get; set; }
        public int CD_FNE { get; set; }
        public int CD_CLS_COD { get; set; }

        public decimal CD_DIV { get; set; }
        public string NO_DIV { get; set; }
        public decimal CD_DPT { get; set; }
        public string NO_DPT { get; set; }
        public decimal CD_SDT { get; set; }

        public string NO_SDT { get; set; }
        public string NO_CLS { get; set; }
        public string NOCLSCOD { get; set; }
    }
}
