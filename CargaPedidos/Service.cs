﻿using Dapper;
using CargaPedidos.Interfaces;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Linq;

namespace CargaPedidos
{
    public class Service : IService
    {
        public ServiceModel GetData(string connectionString, string scriptSql)
        {
            Console.WriteLine("Pegando pedido...");
            using (OracleConnection con = new OracleConnection(connectionString))
            {
                Console.WriteLine("Conectando com o banco...");
                //return con.Query<ServiceModel>(scriptSql).SingleOrDefault();
                var result = con.Query<ServiceModel>(scriptSql).SingleOrDefault();
                return result;
            }
        }

        public void InsertData(string connectionString, string scriptSql, ServiceModel model)
        {
            Console.WriteLine("Inserindo pedido...");

            using (OracleConnection con = new OracleConnection(connectionString))
            {
                Console.WriteLine("Conectando com o banco...");
                try
                {
                    con.Execute(scriptSql, model);
                    Console.WriteLine("PEDIDO inserido com sucesso.");
                }
                catch (Exception)
                {
                    Console.WriteLine("Pedido já consta no bd ou houve um erro.");
                    return;
                }
            }
        }
    }
}
