﻿using CargaPedidos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos
{
    public class TvenEntregaLoopModel : ITvenEntregaLoopModel
    {
        public decimal NI_ETG { get; set; }
        public decimal NI_SKU { get; set; }
        public int NI_TRS { get; set; }
        public int NI_TIP_ETG { get; set; }
        public string NO_TIP_ETG { get; set; }
        public decimal VL_ETG { get; set; }
        public DateTime? DT_EST_ETG { get; set; }
        public int NU_NF { get; set; }
        public decimal NI_PED { get; set; }
        public string NU_ETG_EST { get; set; }
        public string CD_COMPANY { get; set; }
    }
}
