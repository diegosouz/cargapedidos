﻿using CargaPedidos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CargaPedidos
{
    public class ClientePedidoModel : IClientePedidoModel
    {
        public decimal NI_CLI_PED { get; set; }
        public decimal NI_PED { get; set; }
        public string DS_EMAIL_PLTF { get; set; }
        public string DS_EMAIL { get; set; }
        public string CD_USR_VTEX { get; set; }
        public string NO_PRI_NOM { get; set; }
        public string NO_ULT_NOM { get; set; }
        public string CD_DOC { get; set; }
        public string NU_TEL { get; set; }
        public decimal NI_PES { get; set; }
        public decimal NI_VLD_SYNCHRO { get; set; }
    }
}
