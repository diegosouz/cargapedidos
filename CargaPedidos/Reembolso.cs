﻿using CargaPedidos.Interfaces;
using Dapper;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Linq;

namespace CargaPedidos
{
    public class Reembolso : IReembolso
    {
        public ReembolsoModel GetData(string connectionString, string scriptSql)
        {
            using (OracleConnection con = new OracleConnection(connectionString))
            {
                var result = con.Query<ReembolsoModel>(scriptSql).SingleOrDefault();
                return result;
            }
        }

        public void InsertData(string connectionString, string scriptSql, ReembolsoModel model)
        {
            using (OracleConnection con = new OracleConnection(connectionString))
            {
                try
                {
                    con.Execute(scriptSql, model);
                }
                catch (Exception)
                {
                    return;
                }
            }
        }
    }
}
