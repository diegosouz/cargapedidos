﻿using CargaPedidos.Interfaces;
using System;

namespace CargaPedidos
{
    public class ReembolsoModel : IReembolsoModel
    {
        public int? NI_REEM { get; set; }
        public int NI_PED { get; set; }
        public DateTime? DT_SOL { get; set; }
        public int NI_REEM_STA { get; set; }
        public int NI_REEM_TIP { get; set; }
        public char? FL_ORI_LAN { get; set; }
        public decimal VL_REEM { get; set; }
        public int CD_PRT { get; set; }
        public int NI_NF { get; set; }
        public string CD_USR { get; set; }
        public DateTime? DT_INC { get; set; }
        public DateTime? DT_ALT { get; set; }
        public string NU_DOC_SAP { get; set; }
        public string NU_RET_SAP { get; set; }
        public char? FL_FIEL { get; set; }
        public string NU_RET_ADQUIRENTE { get; set; }
        public char? FL_CLI_CON { get; set; }
    }
}
